#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>
//#include <fcntl.h>
#include <unistd.h>

int main( int argc, char* argv[])
{
	int	socketfd, portNumber, length;
	char	readBuffer[2000], message[255];
	struct sockaddr_in serverAddress;	// describes endpoint to connect socket
	struct hostent *server;			// stores information about host nme

	// The command string for a HTTP request to get / (often index.html)
	sprintf(message, "GET / HTTP/1.1\r\nHost: %s \r\nConnection: close\r\n\r\n", argv[1]);
	printf("Sending the message: %s", message);
	
	if (argc <= 1) {
		printf("Incorrect usage, use: ./webbrowser hostname\n");
		return 2;
	}
	// The gethostbyname acepts a string name and returns  host name structure
	if ((server = gethostbyname(argv[1])) == NULL)
	{
		perror("Socket Client: error - unable to resolve host nme.\n");
		return 1;
	}
	
	// Crete the socket of IP address type. SOCK_STREAM for TCP connections
	if ((socketfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("SOcket Client: error opening TCP IP-based socket.\n");
		return 1;
	}
	// Clear the data in the serverAddress sockaddr_in struct
	bzero((char *) &serverAddress, sizeof(serverAddress));
	portNumber = 80;
	serverAddress.sin_family = AF_INET;		// set the address family to be IP
	serverAddress.sin_port = htons(portNumber);	// set port number to 80
	bcopy((char *)server->h_addr,(char *) &serverAddress.sin_addr.s_addr, server->h_length); // set addre to the resolve hostname address
	
	// Try to connect to the server
	if (connect(socketfd, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0) {
		perror("Socket Client: error connecting to the server.\n");
		return 1;
	}
	
	// Send the HTTP request string
	if (write(socketfd, message, sizeof(message)) < 0) {
		perror("Socket Client: error writing to socket");
		return 1;
	}
	
	// Read the HTTP response to a maximum of 2000 characters
	if (read(socketfd, readBuffer, sizeof(readBuffer)) < 0) {
		perror("Socket Client: error reading from socket");
		return 1;
	}
	printf("**START**\n%s\n**END**\n", readBuffer);	// display response
	close(socketfd);				// close the socket
	return 0;
}
/* In this example, the somple web pge from the local RPi Nginx web server
* is requested, by using localhost, which  esentilly means "this device" and
* it uses the Linux loopback virtual network interface (lo) which hes the IP
* address 127.0.0.1
*/
