### A C/C++ Web Client

Full C/C++ support for socket communication can be added to a program by including the `sys/socket.h` header file. In addition, the `sys/types.h` header file contains the data types that are used in system calls, and the `netint/in.h` header file cintains the structure needed for working with Internet domain addresse.

The `webbrowser.c` is the C code for a basic web browser application that can be used to cinnect to a HTTP web server, retrieve a web page, and display it in raw HTML form -- like a regular web browser, but without the pretty rendering. The code performs the following steps:

1. The server name is passed to the program as a string argument. The progrm converts this string into an IP address (stored in the `hostent` structure) using the `gethostbyname()` function
2. The client creates a TCP socket using the `socket()` system call.
3. The `hostent` structure and port number `(80)` are used to create  `sockaddr_in` structure tht specifies the endpoint address to which to connect the socket. This structure also sets the address family to be IP-based (`AF_INET`) and the network byte order.
4. The TCP socket is connected to the server using the `connect()` system call; the communications channel is now open.
5. An HTTP request is sent to the server using the `write()` system call and a fixed-length response is read  from the server using the `read()` system call. The HTML response is displayed.
6. The client disconnects and the socket is closed using `close()`. 
