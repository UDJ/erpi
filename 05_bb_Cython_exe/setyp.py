# Derek Molloy
# Exploring BeagleBone. Tools and Tecnics 
# -2019-2e
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext


ext_modules = [Extension("test", ["test.pyx"])]
setup(
    name = 'random number sum application',
    cmdclass = {'build_ext' : build_ext },
    ext_modules = ext_modules
)

"""
python setup.py build_ext --inplace
# In case "fatal error: Python.h: No such file or directory" then:
sudo apt install python-dev  && ls /usr/lib/*linux*/libpython*.so
"""
