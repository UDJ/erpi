#ifndef SPIDEVICE_H_
#define SPIDEVICE_H_
#include <string>
#include <stdint.h>
#define SPI_PATH "/dev/spidev" /**< The general path to an SPI device **/

namespace exploringRPi {
/**
 * @class SPIDevice
 * @breif Generic SPI Device class that can be used to connect to any type of I2C device and read or write to its registers
 */
class SPIDevice {
public:
	/// The SPI Mode
	enum SPIMODE{
		MODE0 = 0,	//!< Low at idle, capture on rising clock edge
		MODE1 = 1,	//!< Low at idle, capture on falling clock edge
		MODE2 = 2,	//!< High at idle, captude on falling clock edge
		MODE3 = 3	//!< High at idle, capture on rising clock edge
	};
public:
	SPIDevice( unsigned int bus, unsigned int device);
	virtual int open();
	virtual void close();
	virtual int write(unsigned char value);
	virtual int write(unsigned char value[], int length);
	virtual int writeRegister(unsigned int registerAddress, unsigned char value);
	virtual unsigned char readRegister(unsigned int registerAddress);
	virtual unsigned char* readRegisters(unsigned int number, unsigned int fromAddress=0);
	virtual void debugDumpRegisters(unsigned int number = 0xff);
	virtual int setSpeed(uint32_t speed);
	virtual int setMode(SPIDevice::SPIMODE mode);
	virtual int setBitsPerWord(uint8_t bits);
	virtual int transfer(unsigned char read[], unsigned char write[], int length);
	virtual ~SPIDevice();
private:
	std::string filename;	//!< The precise filename for the SPI device
	int file;		//!< The file handleto the device
	SPIMODE mode;		//!< The SPI mode as per the SPIMODE enumeration
	uint8_t bits;		//!< The number of bits per word
	uint32_t speed;		//!< The speed of transfer in Hz
	uint16_t delay;		//!< The tansfer delay in usecs
};

} /* namespace exploringRPi */

#endif /* SPIDEVICE_H_ */
