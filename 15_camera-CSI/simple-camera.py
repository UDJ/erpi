# Using a CSI camera The RPi2 CSI camera with using OpenCV
import cv2
import numpy as np

cam=cv2.VideoCapture("v4l2src ! image/jpeg, width=1920, height=1080,framerate=30/1 ! jpegdec ! videoconvert ! video/x-raw, format=BGR ! appsink ")
while(1):
        ok,img=cam.read()
        if not ok :
                print "failed to read image"
                break
        cv2.imshow("image",img)
        k=cv2.waitKey(1)
        if k==ord('q'):
                break
cv2.destroyAllWindows()
