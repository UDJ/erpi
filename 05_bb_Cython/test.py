# Derek Molloy
# Exploring BeagleBone. Tools and Tecnics 
# -2019-2e
from math import sin


def integrate_sin(a,b,N):
    dx = (b-a)/N
    sum = 0
    for i in range(0,N):
        sum += sin(a+i*dx)
    return sum*dx

"""
python2.7

>>> from math import pi
>>> execfile('test.py')
>>> integrate_sin(0,pi,1000)
>>> integrate_sim(0,pi,1000000)
# For evaluete its performance use:
>>> import timeit
>>> print(timeit.timeit("integrate_sin(0, 3.14159, 1000000)", setup="from __main__ import integrate_sin", number=10))
quit()
# Use Cython
sudo apt install cython
cython -a Molloy_test.py
firefox *.html # The dacker yellow on a line in the .html report, the grater level of dynamic behavior that is taking place on that line

"""
