#-------------------------------------------------
#
# Project created by QtCreator 2019-05-22T10:38:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 14_QtWeather
TEMPLATE = app
# my addition
LIBS += -lwiringPi

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
