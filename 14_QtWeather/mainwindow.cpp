#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDateTime>
#include <wiringPi.h>
#include <unistd.h>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  this->isFahrenheit = false;
  statusBar()->showMessage("Sensor Application Started");
  this->maxTemperature = -100.0f;       // initial values
  this->minTemperature = 100.0f;
  this->updateDisplay();                // refresh UI values (below)
  this->timer = new QTimer(this);       // create the timer

  // When the timer times out, call the on_timerUpdate() function
  connect(timer, SIGNAL(timeout(), this, SLOT(on_timerUpdate()));
  this->timer->start(5000);             // time out after 5 sec
}

float MainWindow::celsiusToFahrenheit(float valueCelsius)
{
  return ((valueCelsius * (9.0f/5.0f)) + 32.0f);
}

void MainWindow::on_getSample_clicked() // calles when button pressed
{
  QDateTime local(QDateTime::currentDateTime());  // display sample time
  statusBar()->showMessage(QString("Update:").append(local.toString()));
  this->readDHTSensor();
  if (temperature < minTemperature)
    minTemperature = temperature;     // min?
  if (temperature > maxTemperature)
    maxTemperature = temperature;     // max?
  this->updateDisplay();
}

void MainWindow::on_timerUpdate()
{
  this->on_getSample_clicked();
  this->updateDisplay();
}

void MainWindow::updateDisplay()
{
  if (this->isFahrenheit) {           // in Fahrenheit mode?
      ui->lcdTemperature->display(celsiusToFahrenheit(temperature));
      ui->temperatureUnits->setText("F"); // set the label to F
  }
  else
  {
      ui->lcdTemperature->display((double)temperature);
      ui->temperatureUnits->setText("C");
  }
  ui->lcdHumidity->display((double)humidity);
  ui->minTemperature->setText(QString::number(minTemperature));
  ui->maxTemperature->setText(QString::number(maxTemperature));
}

void MainWindow::on_radioButton_toggled(bool checked)
{
  this->isFahrenheit = checked;
  this->updateDisplay();
}

MainWindow::~MainWindow()
{
  delete ui;
}

int MainWindow::readDHTSensor()
{

}
