#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

#define USING_DHT11 false   // The DHT11 uses only 8 bits
#define DHT_GPIO      22    // Using GPIO 22 at the schematic
#define LH_THREAHOLD  26    // Low=~14, High=~38 - pick avg

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
private slots:
  void on_getSample_clicked();  // when button is pressed
  void on_radioButton_toggled(bool chacked);  // when radio clicked
  void on_timerUpdate();        // when timer timers out
private:
  float temperature, humidity;  // states
  float maxTemperature, minTemperature;
  bool isFahrenheit;
  QTimer *timer;                // pointer to timer
  void updateDisplay();         // sets the UI values
  int readDHTSensor();          // read DHT sensor
  float celsiusToFahrenheit(float valueCelsius);
  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
