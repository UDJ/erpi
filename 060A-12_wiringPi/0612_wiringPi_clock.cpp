#include <iostream>
#include <wiringPi.h>

using namespace std;
#define GPCLK0 4		// this is Pin 7 GPIO4
#define GPCLK1 5		// Pin 29, GPIO5 -- do not use
#define GPCLK2 6 	// Pin 31, GPIO6 -- RPi A+,B+,2/3

int main() {	// must be run as root
	wiringPiSetupGpio();		// use the GPIO numbers
	pinMode(GPCLK0, GPIO_CLOCK);	// set up the clock from 19.2MHz base
	gpioClockSet(GPCLK0, 4800000);	// output a clean 4.8MHz clock on GPCLK0
	cout << "The clock output is enabled on GPIO" << GPCLK0 << endl;
	return 0; // clock persists after exit
}
