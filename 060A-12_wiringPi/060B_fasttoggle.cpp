// Do not optimize this code usin -O3 s it will revove the delay hack
#include <iostream>
#include <wiringPi.h>

using namespace std;
#define LED_GPIO 17 	// this is GPIO17, Pin 11

int main() {
	wiringPiSetupGpio(); 		// use the GPIO, not WPi, labels
	cout << "Starting fast GPIO toggle on GPIO" << LED_GPIO << endl;
	cout << "Press CTRL+CC to quit..." << endl;
	pinMode(LED_GPIO, OUTPUT); 	// the LED set up as an output
	while(1) { // loop forever - await ^C press
		digitalWrite(LED_GPIO, HIGH); // LED on
		for(int i=0; i<50; i++) { } // blocking delay hack
		digitalWrite(LED_GPIO, LOW); // LED off
		for(int i=0; i<49; i++) { } // shorter delay to balance
	} // the duty cycle somewhat
	return 0; // program will never reach here!
}
