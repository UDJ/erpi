#include <iostream>
#include "network/SocketClient.h"

using namespace std;
using namespace erpi;

int main(int argc, char *argv[])
{
	if (argc != 2) {
		cout << "Incorrect usage: " << endl;
		cout << "	client server_name" << endl;
		return 2;
	}
	
	cout << "Starting RPi Client Example" << endl;
	SocketClient sc(argv[1], 54321);
	sc.connectToServer();
	string message("Hello from the Client Example");
	
	cout << "Sending [" << message << "]" << endl;
	sc.send(message);
	
	string rec = sc.receive(1024);
	cout << "Received [" << rec << "]" << endl;
	
	cout << "End of RPi Client Example" << endl;
	return 0;
}
// For build:
// g++ client.cpp network/SocketClient.cpp -o client