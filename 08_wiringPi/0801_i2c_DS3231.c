#include <wiringPiI2C.h>
#include <stdio.h>

// The time is in the register in decimal form
//int bcdToDec(int b) { return (b/16)*10+(b%16); }
int main() {
	int fd = wiringPiI2CSetup(0x68);
	int secs = wiringPiI2CReadReg8(fd, 0x00);
	int mins = wiringPiI2CReadReg8(fd, 0x01);
	int hours = wiringPiI2CReadReg8(fd, 0x02);
	printf("The RTC time is %02d:%02d:%02d\n", hours,mins,secs);
	return 0;
}
/** gcc DS3231.c -o DS3231_RTC -lwiringPi **/
