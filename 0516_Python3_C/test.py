#!/usr/bin/python3
import ERPiModule
print("*** Start of the Pyhon program")
print("--> Calling the C hello() function passing Pi")
ERPiModule.hello("Pi")
print("--> Calling the C integrate() function")
val = ERPiModule.integrate(0, 3.14159, 1000000)
print("*** he result is: ", val)
print("*** End of the Python program")
