#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

int main(int argc, char *args[])
{
	Mat frame;
	VideoCapture *capture;		// capture need full scope of main()
	cout << "Starting face detection applicator" << endl;

	// Loading image from a file
	if (argc == 2) {
		cout << "Loading the image" << args[1] << endl;
		frame = imread(args[1], CV_LOAD_IMAGE_COLOR);
	}
	else {
		cout << "Capturing from the webcam" << endl;
		capture = new VideoCapture(0);

		// Set any properties in the VideoCapture object
		capture->set(CV_CAP_PROP_FRAME_WIDTH, 1024);
		capture->set(CV_CAP_PROP_FRAME_HEIGHT, 1280);

		// Connect to the camera
		if (!capture->isOpened()) {
			cout << "Failed to connect to the camera." << endl;
			return 1;
		}

		// Populate the frame with captured image
		*capture >> frame;
		cout << "Successfully captured a frame." << endl;
	}

	if (!frame.data) {
		cout << "Invalid image data .. exiting!" << endl;
		return 1;
	}

	// Loading the face classifier from a file (standard OpenCV example)
	CascadeClassifier faceCascade;
	faceCascade.load("haarcascade_frontalface.xml");

	// Faces is a STL vector of faces - will store the detected faces
	std::vector<Rect> faces;

	// Detect objects in the scene using the classifier above (frame,
	//	faces, scale factor, min neighbors, flags, min size, max size)
	faceCascade.detectMultiScale(frame, faces, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(50, 50));

	// Display the image
	if (faces.size() == 0) {
		cout << "No faces detected!" << endl;
	}

	// Draw oval around the detected faces in the faces vector
	for (int i = 0; i < faces.size(); i++) {

		// Using the center point and a rectangle to create an ellipse
		Point cent( faces[i].x + faces[i].width * 0.5,
					faces[i].y + faces[i].height * 0.5 );
		RotatedRect rect( cent, Size(faces[i].width, faces[i].width), 0);

		// Image, rect(cent, color = green, thickness = 3, linetype = 8
		ellipse( frame, rect, Scalar(0,225,0), 3, 8);
		cout << "Face at: (" << faces[i].x << "," << faces[i].y << ")" << endl;
	}

	// Display and save the image results
	imshow("RPi OpenCV face detection", frame);
	imwrite("faceOutput.png", frame);
	waitKey(0);	// - display the image until a key press

	return 0;
}
