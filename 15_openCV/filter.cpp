#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main()
{
	// Capture from /dev/video0
	VideoCapture capture(0);
	cout << "Started Processing - Capturing Image" << endl;

	// Set any properties in the VideoCapture object:
	capture.set(CV_CAP_PROP_FRAME_WIDTH,1280);	// - width in pixels
	capture.set(CV_CAP_PROP_FRAME_HEIGHT,720);	// - height in pixels
	capture.set(CV_CAP_PROP_GAIN, 0);			// - enable auto gain

	// Connect to the camera
	if (!capture.isOpened()) {
		cout << "Failed to connect to the camera" << endl;
	}

	Mat frame, gray, edges;		// original, grayscale and edge image
	capture >> frame;			// capture the image to the frame

	// Did the capture succeed?
	if (frame.empty()) {
		cout << "Failed to capture an image" << endl;
		return -1;
	}
	cout << "Processing - Performing Image Processing" << endl;
	cvtColor(frame, gray, CV_BGR2GRAY);		// convert to gray scale
	blur(gray, edges, Size(3,3));			// blur image using a 3x3 kernel
	// Use Canny adge detector that output to the same image;
	// It have low threshold = 10, high threshold = 30, kernel size = 3
	Canny(edges, edges, 10, 30, 3);			// run Canny edge detector
	cout << "Finished Processing - Saving images" << endl;
	imwrite("capture.png", frame);			// store the original image
	imwrite("grayscale.png", gray);		// store the grayscale image
	imwrite("edges.png", edges);			// store the processed edge image

	return 0;
}
