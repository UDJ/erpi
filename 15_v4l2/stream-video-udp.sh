#!/bin/bash
echo "Video Streaming for RPi"
v4l2-ctl --set-fmt-video=width=1920,height=1080,pixelformat=1
./capture -d /dev/video0 -F -o -c0 | \
avconv -re -i - -vcodec copy -f mpegts \
udp://192.168.1.2:12345
